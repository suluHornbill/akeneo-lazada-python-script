
from akeneo import Akeneo
import re, math, time, datetime

def generate_log(text):
    dt = datetime.date.today()
    with open(f'logs/error-{dt}.log', "a") as log_file:
        log_file.write(f"error => {text}\n")
        log_file.close()

def main():

    lazada_token = "api token key"

    akeneo = Akeneo("my".upper())
    akeneo_token = akeneo.generate_token_by_password()
    print(akeneo_token)
    akeneo_details = akeneo.get_product(akeneo_token, payloads = {
            "page": 1,
            "with_count": 'true',
            "limit": 1
        })
    
    items_count = akeneo_details['items_count'] if 'items_count' in akeneo_details else 0

    tmp_total_page = abs(int(items_count) / 100)
    akeneo_total_page = int(str(tmp_total_page).split('.')[1]) + 1

    limit_page = 10
    counter = 1

    print("total page in akeneo => {} with total record => {}\n".format(akeneo_total_page, items_count))
    while limit_page != 0:
        akeneo_products = akeneo.get_product(akeneo_token, payloads = {
            "page": counter,
            "with_count": 'true',
            "limit": 100
        })

        embedded = akeneo_products['_embedded']['items'] if akeneo_products['_embedded']['items'] !=  None else ""
        for item in embedded:

            try:

                laz_create_xml = akeneo.generate_lazada_xml(item, lazada_token)
                laz_create_message = akeneo.push_to_lazada(lazada_token, laz_create_xml)
                print(laz_create_xml)

                time.sleep(2)
                if 'detail' in laz_create_message and re.search('duplicate', laz_create_message['detail'][0]['message']):
            
                    laz_update_json = akeneo.get_lazada_update_json(lazada_token, item)
                    laz_update_xml = akeneo.generate_lazada_update_xml(laz_update_json)
                    laz_update_message = akeneo.update_product_lazada(lazada_token, laz_update_xml)

                    print("xml update payload => \n")
                    print(laz_update_xml)
                    print("\n")
                    print("update message => ")
                    print(laz_update_message)
                else:
                    print("other message => \n")
                    print(laz_create_message)
                    print("\n")
            
                continue
    
            except Exception as ex:

                with open('logs/error.log', "a") as f:
                    f.write(f'exception => {ex}\n')
                    f.close()
                continue
        
        if limit_page > 1:
            print('next akeneo data....{}\n'.format(counter))
        else:
            print('akeneo data end...')
        counter += 1
        limit_page -= 1
        time.sleep(10)

if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        generate_log(ex)