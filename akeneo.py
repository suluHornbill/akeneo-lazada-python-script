import datetime, decimal, io, json, requests, time, secrets, urllib, os, re, time
from requests.api import request
import requests.auth
import lazop.base as lazop
from http import client
from base64 import b64encode, b64decode

# https://blog.confirm.ch/mastering-vim-opening-files/
# https://community.bitnami.com/t/akeneo-parameter-client-id-is-missing-or-does-not-match-any-client-or-secret-is-invalid/52442/4
# https://stackoverflow.com/questions/36719540/how-can-i-get-an-oauth2-access-token-using-python


class Akeneo:
    
    def __init__(self, country):
        # akeneo details
        self.akeneo_host_url = 'akeneo api hostname url'
        self.akeneo_https_conn = client.HTTPSConnection("api hostname")
        self.akeneo_client_id = 'akeneo client id'
        self.akeneo_secret_key = 'akeneo secret key'
        self.akeneo_username = 'akeneo username'
        self.akeneo_password = 'akeneo password'
        
        # other 
        # check for url api endpoint based country 
        if country == "MY":
            self.currency_type = "MYR"
            self.lazada_api_endpoint = "lazada api url"
        elif country == "SG":
            self.currency_type = "SGD"
            self.lazada_api_endpoint = "lazada api url"
        elif country == "PH":
            self.currency_type = "PHP"
            self.lazada_api_endpoint = "lazada api url"
        elif country == "TH":
            self.currency_type = "THB"
            self.lazada_api_endpoint = "lazada api url"
        elif country == "ID":
            self.currency_type = "IDR"
            self.lazada_api_endpoint = "lazada api url"
        elif country == "VN":
            self.currency_type = "VND"
            self.lazada_api_endpoint = "lazada api url"
        else:
            self.currency_type = "MYR"
            self.lazada_api_endpoint = "lazada api url"
        
        # lazada details
        self.lazada_rest_url = self.lazada_api_endpoint
        self.lazada_auth_url = "lazada rest api"
        self.lazada_code = "lazada code"
        self.lazada_key = "lazada key"
        self.lazada_secret = "lazada secret"

    def generate_token_by_password(self):
        encode_up = b64encode("{}:{}".format(self.akeneo_client_id, self.akeneo_secret_key).encode('utf-8'))
        conn = self.akeneo_https_conn
        payloads = {
            "username": self.akeneo_username,
            "password": self.akeneo_password,
            "grant_type": "password"
        }
        
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Basic {}'.format(encode_up.decode('utf-8'))
        }
        
        conn.request("POST", "/api/oauth/v1/token", body=json.dumps(payloads), headers=headers)
        res = conn.getresponse()
        data = res.read()
        json_data = data.decode('utf-8')
        return json.loads(json_data)['access_token'] if 'access_token' in json.loads(json_data) else json.loads(json_data)['message']
    
    def generate_refresh_token(self, token): 
        encode_up = b64encode("{}:{}".format(self.akeneo_client_id, self.akeneo_secret_key).encode('utf-8'))
        conn = self.akeneo_https_conn
        
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Basic {}".format(encode_up.decode('utf-8'))
        }
        
        payloads = {
            "grant_type": "refresh_token",
            "refresh_token": token
        }
        
        conn.request("POST", "/api/oauth/v1/token", body=json.dumps(payloads), headers=headers)
        res = conn.getresponse()
        data = res.read()
        json_data = data.decode('utf-8')
        
        if "code" in json.loads(json_data):
            return json.loads(json_data)['message']
        else:
            if "access_token" in json.loads(json_data):
                return json.loads(json_data['access_token'])
            else:
                return json.loads(json_data['refresh_token'])
        
    def get_product(self, token, payloads):
        
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer {}'.format(token)
        }
        
        api_url = self.akeneo_host_url + '/rest/v1/products'
        req = requests.get(api_url, params=payloads, headers=headers)

        if req.status_code == 404:
            return "error 404: invalid url api"
        else:
            json_data = json.loads(req.content)
            return json_data
    
    # == lazada related function == 
    def generate_lazada_token(self):
        
        client = lazop.LazopClient(self.lazada_auth_url, self.lazada_key, self.lazada_secret)
        request = lazop.LazopRequest('/auth/token/create','GET')
        request.add_api_param('code', self.lazada_code)
    
        response = client.execute(request)
        result = response.body
        return result['access_token'] if 'access_token' in result else result
    
    def get_lazada_category_tree(self, token, suggestion): 
        
        try:
            
            client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
            request = lazop.LazopRequest('/category/tree/get', 'GET')
            request.add_api_param("product_name", suggestion)
            response = client.execute(request, token)
        
            result = response.body
       
            children = result['data'][0]['children'][0]['children'][0]['category_id'] if 'category_id' in result['data'][0]['children'][0] and type(result['data'][0]['children']) is not None.__class__ else []        
            return children
        except KeyError:
            pass

    def get_lazada_category_suggestion(self, token, suggestion):
        try:
            client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
            request = lazop.LazopRequest('/product/category/suggestion/get', 'GET')
            request.add_api_param("product_name", suggestion)
            response = client.execute(request, token)

            result = response.body
            return result['data']['categorySuggestions'][0]['categoryId']
        
        except KeyError:
            pass

    def return_single_price(self, data):
        if 'price' in data and type(data) is list:
            single_price = ""
            for i in range(0, len(data)):
                if data[i]['amount'] == '0.00':
                    pass
                elif data[i]['amount'] != '0.00' and data[i]['currency'] == self.currency_type:
                    single_price = "{}".format(data[i]['amount'])
            return single_price
        else:
            pass 
    
    def generate_lazada_xml(self, data, token = ""):
        
        # sku = data['_embedded']['items'][0]['identifier'] if data['_embedded']['items'][0]['identifier'] != None else ""
        sku = data['identifier'] if data['identifier'] != None else ""
        product_name = data['values']['name'][0]['data'] if 'name' in data['values'] and data['values']['name'][0]['data'] != None else ""
        
        # category_id = data['categories'][0] if data['categories'][0] != None else ""
        category_id = self.get_lazada_category_suggestion(token, product_name)
        
        # parent = data['parent'] if data['parent'] != None else ""
        # family = data['family'] if data['family'] != None else ""
        nike_price = data['values']['price'][0]['data'] if 'price' in data['values'] and data['values']['price'][0]['data'] != None else []
        # product_size = data['values']['size'][0]['data'] if data['values']['size'][0]['data'] != None else ""
        color = data['values']['color'][0]['data'] if 'color' in data['values'] and data['values']['color'][0]['data'] != None else ""
        # nike_gender = data['values']['nike_gender'][0]['data'] if data['values']['nike_gender'][0]['data'] != None else ""
        # nike_gender_age = data['values']['nike_gender_age'][0]['data'] if data['values']['nike_gender_age'][0]['data'] != None else ""
       
        short_description = data['values']['short_description'][0]['data'] if 'short_description' in data['values'] and data['values']['short_description'][0]['data'] != None else None
        description = data['values']['description'][0]['data'] if 'description' in data['values'] and data['values']['description'][0]['data'] != None else ""
        
        # image
        image_1 = data['values']['image_link_1'][0]['data'] if 'image_link_1' in data['values'] and len(data['values']['image_link_1'][0]['data']) > 0 else ""
        image_2 = data['values']['image_link_2'][0]['data'] if 'image_link_2' in data['values'] and len(data['values']['image_link_2'][0]['data']) > 0 else ""
        image_3 = data['values']['image_link_3'][0]['data'] if 'image_link_3' in data['values'] and len(data['values']['image_link_3'][0]['data']) > 0 else ""
        image_4 = data['values']['image_link_4'][0]['data'] if 'image_link_4' in data['values'] and len(data['values']['image_link_4'][0]['data']) > 0 else ""
        image_5 = data['values']['image_link_5'][0]['data'] if 'image_link_5' in data['values'] and len(data['values']['image_link_5'][0]['data']) > 0 else ""
        image_6 = data['values']['image_link_6'][0]['data'] if 'image_link_6' in data['values'] and len(data['values']['image_link_6'][0]['data']) > 0 else ""
        image_7 = data['values']['image_link_7'][0]['data'] if 'image_link_7' in data['values'] and len(data['values']['image_link_7'][0]['data']) > 0 else ""
        image_8 = data['values']['image_link_8'][0]['data'] if 'image_link_8' in data['values'] and len(data['values']['image_link_8'][0]['data']) > 0 else ""
        image_9 = data['values']['image_link_9'][0]['data'] if 'image_link_9' in data['values'] and len(data['values']['image_link_9'][0]['data']) > 0 else ""
        image_10 = data['values']['image_link_10'][0]['data'] if 'image_link_10' in data['values'] and len(data['values']['image_link_10'][0]['data']) > 0 else ""
        
        images = self.return_image_list(image_1, image_2, image_3, image_4, image_5, image_6, image_7, image_8, image_9, image_10)  
        
        name_xml = "<name>{}</name>".format(product_name) if product_name != None else "</name>"
        short_description_xml = "<short_description>{}</short_description>".format(short_description) if short_description != None else "<short_description />"
        product_description_xml = "<description>{}</description>".format(description) if description != None else "<description />"
        color_xml = "<color_family>{}</color_family>".format(color)
        brand_xml = "<brand>Nike</brand>"
        model_xml = "<model>test</model>"
        kid_years_xml = "<kid_years />"
        warranty_type_xml = "<warranty_type>No Warranty</warranty_type>"
        warranty_period_xml = "<warranty_period>12 Month</warranty_period>"
        warranty_policy_xml = "<warranty_policy>None</warranty_policy>"
            
        # xml_price = [x for x in nike_price if x['amount'] != '0.00' and x['currency'] == self.currency_type][0]['amount']
        try:
            xml_price = self.return_single_price(nike_price)
        except Exception as ex:
            print(f'return single price exception => {ex}')
            pass
        
        package_length = data['values']['package_length'][0]['data']['amount'] if 'package_length' in data['values'] and data['values']['package_length'][0]['data']['amount'] != "" else ""
        package_height = data['values']['package_height'][0]['data']['amount'] if 'package_height' in data['values'] and data['values']['package_height'][0]['data']['amount'] != "" else ""
        package_weight = data['values']['package_weight'][0]['data']['amount'] if 'package_weight' in data['values'] and data['values']['package_weight'][0]['data']['amount'] != "" else ""
        package_width = data['values']['package_width'][0]['data']['amount'] if 'package_width' in data['values'] and data['values']['package_width'][0]['data']['amount'] != "" else ""
        
        xml_sku = ""
        if type(data['values']['size']) is list:
            product_size_length = len(data['values']['size'])
            if product_size_length > 0:
                xml_sku += "<Skus>"
                for index in range(0, len(data['values']['size'])):
                    
                    index_name = "-{}".format(index) if index == 1 or index > 1 else ""
                    
                    seller_sku = "<SellerSku>{}{}</SellerSku>".format(sku, index_name)
                    color_sku = "<color_family>{}</color_family>".format(color)
                    size_sku = "<size>{}</size>".format(data['values']['size'][index]['data'])
                    quantity_sku = " <quantity>1</quantity>"
                    core_construction = "<Core_Construction />"
                    
                    # price_sku = "<price>{}</price>".format(nike_price[item]['amount'])
                    price_single_sku = "<price>{}</price>".format(xml_price) 
                    special_price_sku = "<special_price />"
                    special_from_date_sku = "<special_from_date />"
                    special_to_date_sku = "<special_to_date />"
                    package_length_sku = "<package_length>{}</package_length>".format(package_length)
                    package_height_sku = " <package_height>{}</package_height>".format(package_height)
                    package_weight_sku = "<package_weight>{}</package_weight>".format(package_weight)
                    package_width_sku = "<package_width>{}</package_width>".format(package_width)
                    package_content_sku = "<package_content>1 shoes</package_content>"
                    
                    images_xml = ""
                    if len(images) != 0:
                        images_xml += "<Images>"
                        for i in range(0, len(images)):
                            images_xml += "<Image>{}</Image>".format(images[i])
                        images_xml += "</Images>"
                    else:
                        images_xml += "<Images />"
                    
                    xml_sku += "<Sku>{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}</Sku>".format(seller_sku, color_sku, size_sku, quantity_sku, core_construction, price_single_sku, special_price_sku, special_from_date_sku, special_to_date_sku, package_length_sku, package_height_sku, package_weight_sku, package_width_sku, package_content_sku, images_xml)
                    
                xml_sku += "</Skus>"
            else:
                xml_sku += "<Skus />"
                
        xml = ""
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        xml += "<Request><Product>"
        xml += "<PrimaryCategory>{}</PrimaryCategory>".format(category_id)
        xml += "<SPUId />"
        xml += "<AssociatedSku />"
        xml += "<Attributes>{}{}{}{}{}{}{}{}{}{}</Attributes>".format(name_xml, short_description_xml, product_description_xml, color_xml, brand_xml, model_xml, kid_years_xml, warranty_type_xml, warranty_period_xml, warranty_policy_xml)
        xml += "{}".format(xml_sku)
        xml += "</Product></Request>"

        return xml
    
    def get_lazada_product(self, token, payload):
        client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
        request = lazop.LazopRequest('/products/get', 'GET')
        request.add_api_param('filter', payload['filter'])
         
        if 'limit' in payload:
            request.add_api_param('limit', payload['limit'])
        elif 'sku_seller_list' in payload:
            request.add_api_param('sku_seller_list', payload['sku_seller_list'])
            
        response = client.execute(request, token)
        result = response.body
        return result
    
    def update_product_lazada(self, token, xml):
        client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
        request = lazop.LazopRequest('/product/update')
        request.add_api_param('payload', xml)
        
        response = client.execute(request, token)
        result = response.body
        
        return result
    
    def push_to_lazada(self, token, xml):
        client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
        request = lazop.LazopRequest('/product/create')
        request.add_api_param('payload', xml)
        response = client.execute(request, token)
    
        result = response.body
        return result
    
    def return_image_list(self, img1 = "", img2 = "", img3 = "", img4 = "", img5 = "", img6 = "", img7="", img8="", img9="", img10=""):
    
        img_list = []
    
        img_list.append(img1)
        img_list.append(img2)
        img_list.append(img3)
        img_list.append(img4)
        img_list.append(img5)
        img_list.append(img6)
        img_list.append(img7)
        img_list.append(img8)
        img_list.append(img9)
        img_list.append(img10)
        new_list = [a for a in img_list if a != ""]
        return new_list
    
    def get_lazada_category_attributes(self, token, primary_category_id):
        client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key , self.lazada_secret)
        request = lazop.LazopRequest('/category/attributes/get', 'GET')
        request.add_api_param('primary_category_id', primary_category_id)
        response = client.execute(request, token)

        result = response.body
        return result
    
    # ==== update lazada ==== #
    def generate_lazada_update_xml(self, data):
        
        item_id = data['Product']['ItemId']
        name = data['Product']['Attributes']['name']
        short_desc = data['Product']['Attributes']['short_description']
        desc = data['Product']['Attributes']['description']
        skus = data['Product']['Skus']
        
        sku_xml = ""
        if len(skus) > 0:
            
            for sku in skus:
                
                image_xml = ""
                if len(sku['Images']):
                    image_xml += "<Images>"
                    for img in sku['Images']:
                        image_xml += "<Image>{}</Image>".format(img)
                    image_xml += "</Images>"
                else:
                    image_xml = "<Images/>"
                
                sku_xml += "<Sku>"
                sku_xml += "<SkuId>{}</SkuId>".format(sku['SkuId'])
                sku_xml += "<SellerSku>{}</SellerSku>".format(sku['SellerSku'])
                sku_xml += "<quantity>{}</quantity>".format(sku['quantity'])
                sku_xml += "<price>{}</price>".format(sku['price'])
                sku_xml += "<package_length>{}</package_length>".format(sku['package_length'])
                sku_xml += "<package_height>{}</package_height>".format(sku['package_height'])
                sku_xml += "<package_weight>{}</package_weight>".format(sku['package_weight'])
                sku_xml += "<package_width>{}</package_width>".format(sku['package_width'])
                sku_xml += "{}".format(image_xml)
                sku_xml += "</Sku>"
        else:
            sku_xml += "<Sku />"
        
        xml = ""
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
        xml += "<Request>"
        xml += "<Product>"
        xml += f"<ItemId>{item_id}</ItemId>"
        xml += "<Attributes>"
        xml += "<name>{}</name>".format(name)
        xml += "<short_description>{}</short_description>".format(short_desc)
        xml += "<description>{}</description>".format(desc)
        xml += "<delivery_option_sof>No</delivery_option_sof>"
        xml += "</Attributes>"
        xml += "<Skus>{}</Skus>".format(sku_xml) 
        xml += "</Product>"
        xml += "</Request>"
        
        return xml.strip()
    
    def get_lazada_update_json(self, token, akeneo_data):
        
        product_name = akeneo_data['values']['name'][0]['data'] if 'name' in akeneo_data['values'] else ""
        short_desc = akeneo_data['values']['short_description'][0]['data'] if 'short_description' in akeneo_data['values'] else ""
        desc = akeneo_data['values']['description'][0]['data'] if 'description' in akeneo_data['values'] else "" 
        size_lists = akeneo_data['values']['size'] if 'size' in akeneo_data['values'] else []
        price_list = akeneo_data['values']['price'][0]['data'] if 'price' in akeneo_data['values'] else []
        
        # image from akeneo
        img1 = akeneo_data['values']['image_link_1'][0]['data'] if 'image_link_1' in akeneo_data['values'] and len(akeneo_data['values']['image_link_1'][0]['data']) > 0 else ""
        img2 = akeneo_data['values']['image_link_2'][0]['data'] if 'image_link_2' in akeneo_data['values'] and len(akeneo_data['values']['image_link_2'][0]['data']) > 0 else ""
        img3 = akeneo_data['values']['image_link_3'][0]['data'] if 'image_link_3' in akeneo_data['values'] and len(akeneo_data['values']['image_link_3'][0]['data']) > 0 else ""
        img4 = akeneo_data['values']['image_link_4'][0]['data'] if 'image_link_4' in akeneo_data['values'] and len(akeneo_data['values']['image_link_4'][0]['data']) > 0 else ""
        img5 = akeneo_data['values']['image_link_5'][0]['data'] if 'image_link_5' in akeneo_data['values'] and len(akeneo_data['values']['image_link_5'][0]['data']) > 0 else "" 
        img6 = akeneo_data['values']['image_link_6'][0]['data'] if 'image_link_6' in akeneo_data['values'] and len(akeneo_data['values']['image_link_6'][0]['data']) > 0 else ""
        img7 = akeneo_data['values']['image_link_7'][0]['data'] if 'image_link_7' in akeneo_data['values'] and len(akeneo_data['values']['image_link_7'][0]['data']) > 0 else ""
        img8 = akeneo_data['values']['image_link_8'][0]['data'] if 'image_link_8' in akeneo_data['values'] and len(akeneo_data['values']['image_link_8'][0]['data']) > 0 else ""
        img9 = akeneo_data['values']['image_link_9'][0]['data'] if 'image_link_9' in akeneo_data['values'] and len(akeneo_data['values']['image_link_9'][0]['data']) > 0 else ""
        img10 = akeneo_data['values']['image_link_10'][0]['data'] if 'image_link_10' in akeneo_data['values'] and len(akeneo_data['values']['image_link_10'][0]['data']) > 0 else ""
        img_list = self.return_image_list(img1, img2, img3, img4, img5, img6, img7, img8, img9, img10)
        # end of image from akeneo
         
        # == lazada api ==  
        laz_payloads = {
            'filter': 'all',
            'limit': 1,
            'sku_seller_list': '[ \"{}\" ]'.format(product_name)  
        }
        
        laz_product = self.get_lazada_product(token, laz_payloads)
        item_id = laz_product['data']['products'][0]['item_id'] if 'item_id' in laz_product['data']['products'][0] else ""
        skus = laz_product['data']['products'][0]['skus'] if 'skus' in laz_product['data']['products'][0] else []
        # == end of lazada api ==
        
        all_sku = []
        if len(size_lists) > 0:
            for index in range(0, len(size_lists)):
                sku_id = skus[index]['SkuId']
                seller_sku = skus[index]['SellerSku']
                quantity = 1
                package_length = akeneo_data['values']['package_length'][0]['data']['amount']
                package_height = akeneo_data['values']['package_height'][0]['data']['amount']
                package_weight = akeneo_data['values']['package_weight'][0]['data']['amount']
                package_width =  akeneo_data['values']['package_width'][0]['data']['amount']
                
                tmp_sku = {
                    'SkuId': sku_id,                
                    'SellerSku': seller_sku,                 
                    'quantity': quantity,        
                    'price': self.return_single_price(price_list),                 
                    'package_length': package_length,                 
                    'package_height': package_height,                 
                    'package_weight': package_weight,                
                    'package_width': package_width,
                    'Images': img_list if len(img_list) > 0 else []
                }
                
                all_sku.append(tmp_sku)       
        
        result_dic = {
            'Product': {
                'ItemId': item_id,
                'Attributes': {
                    'name': product_name,
                    'short_description': short_desc,
                    'delivery_option_sof': "No",
                    'description': desc,
                },
                'Skus': all_sku
            }
        }
        
        return result_dic
    
    #=============== lazada testing ==================#
    def test(self, data):
        nike_price = data['values']['price'][0]['data'] if data['values']['price'][0]['data'] != None else []
        
        print(data['identifier'])
        
        for i in range(0, len(nike_price)):
            if nike_price[i]['amount'] == '0.00':
                pass
            else:
                print(nike_price[i]['amount'])
    
    def test_push_xml_lazada(self, token, xml):
        
        client = lazop.LazopClient(self.lazada_rest_url, self.lazada_key, self.lazada_secret)
        request = lazop.LazopRequest('/product/create')
        request.add_api_param('payload', xml)
        response = client.execute(request, token)
    
        result = response.body
        return result
